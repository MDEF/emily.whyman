---
title: atlas of weak signals
period: 04 February - 12 April
date: 2019-01-21 12:00:00
term: 2
published: true
---

*Jordi is a psychologist living in rural Catalonia, who is going to visit a biofluencer in Barcelona. He's interested in learning about the effect on social media on mental health as he's doing a gardening workshop with some of his patients next week. The biofluencer is different to your generic instagram pictures of coffee and lotus biscuit, the new generation of biofluencer promotes how to make ecological choices in everyday decisions.*

*Flora, the biofluencer, describes how she has been promoting the matterlabels. The matterlabels are a new labelling system which illustrate how to combine material 'waste' to evolve into a new, useful product.  In order to make your product, the Mattermaker 2.0 must have an input of desired product lifespan, materials currently in possession and their dimensions. The Mattermakers are located around the city centre. For example, Flora described how she had recently created geopolymer - a hard ceramic for tiling (self-healing concrete, heatproof) from sand, bones and fly ash - something she never thought she could make. The matterlabels were derived from the bio-optimiser, an algorithm which was funded due by sanctions on single use and landfill materials. They are similar to a QR code, in which users scan, then go to their nearest Mattermaker in order to produce their product.*

*The bio-optimiser is embedded with an atlas of materials which shows where a product comes from and how it pairs with another product to make something new. Jordi debated on what to make for his workshop, deciding on making small holders for seedlings which would eventually biodegrade into the soil once roots have been developed. Using the matterlabels, Jordi made a nutrient-enriched bio-plastic using starch (from potatoes), glycerin and carbon (charcoal).*

Narrative, written for the final presentation.

# reflections

Researching and compiling the Atlas of Weak Signals gave more depth and meaning to the final project - refreshing to discover new (if depressing at times) topics which concern humanity today. The overarching topic of the **Anthropocene,** but also the topics which are **against** the idea of Western cultural dominance are most related to my project. It is difficult, in my position as a white British woman, to try and argue against Western dominance; something which has been engrained in our everyday lives for so long - and avoiding being biased along the way. However, when studying innovation  non-Western cultures there is a lesson to be learnt. For example, the consumption of insects. Entomorpaghy has been in practice since prehistoric times - yet has not been applied on an industrial ecological scale. Therefore, it is difficult to say what environmental results can appear when producing enough feed our demanding ecosystem. The cultural taboo, or disgust at eating insects in the West can perhaps be linked with colonialism, in the idea that insects are considered *lower* form of food to be eaten. Historically, colonialism provided new and more exotic meats, alongside tea, spices and other things for the wealthy to gorge upon. This plays into an ecological future that is *non-western centric*. With mass globalisation there lies opportunity in discovering new flavours and methods of cooking in the rituals embedded within different cultures. After speaking with Mario, the chef at Leka, he spoke rather poetically about how chefs are using root techniques alongside new cooking technologies to provide modern experiences of dining.  

**"The thought of rising sea levels and more intense heatwaves are enough to keep you up at night. But while we all know the situation is getting more serious, most of us are preoccupied with work, doctor’s appointments and paying bills – and these immediate, visceral worries win every time."**

(*'Would you eat insects to save the planet from global warming?'*, The Guardian)

This quote from The Guardian reflects a guilt which many of us feel in relation to how our everyday lives pan out. We take the car as we're running a little too late in the morning, we get a coffee as it's easier than taking a flask, we treat ourselves to a takeaway as it's been a hard day at work. All these actions often contribute towards an industrial system created which manipulate animals and produce toxic waste. These  all contribute to a persistent feeling of earthly-guilt if we have any remorse towards the ground which has provided sustenance for our complete existence. Further, the article illustrates how 76% of participants ate a protein enriched wormy-truffle when they were told the truffle would make them feel good or look trendy, in comparison to the 62% who ate the truffle when told about their lesser environmental impact than other meat farming. It appears shallow, but the extension of our social activity into a media sphere in the last decade has completely reshaped the way in which we consume and, *how we want to appear when we consume.* The **Biogram** (speculatively created) for the presentation, was a reflection on the progression of social media and new jobs. The Biogram offers potential to harness our feelings of earthly-guilt in conjunction with social media influence in a positive way - through creating a media account which documents 'good deeds' and influences others along the way.. Further, the **Matterlabels**, created by the **Material Optimiser** were an example of how consumerism could be considered positive - in purchasing two products and identifying through their specialised 'footprint', the user can visit their local *Mattermaker* in order to combine and utilise the products into something new. Although looking at materials, the base idea of understanding an object from its fundamental matter is applicable to food production - paring items, or ingredients together to make something beautiful.

The prediction and already occurring effects of a huge increase in forced migration due to the effects of climate change has led to encampments of refugees, in which many are becoming long-term homes of the displaced. The friction between cultures often leads to confusion and violence between cross-cultures, in which finding a job, sanitation and welfare are incredibly difficult. In addressing this, a project which encompasses **context-based agriculture** could be beneficial. For example, a plant-protein incubator which can be created to provide livelihood and skills for displaced farmers would help adjustment to new surroundings. In looking at the hot and humid biome of Barcelona, there is large potential to grow insects. An insect-farm incubator which responds to this biome may also be applicable in other similar temperaments. Due to our wide network of DIY open-source Internet forums, a displaced individual has the potential to begin their own livelihood in their relocated situation, with the help of a smart phone and starter kit.

Perhaps much of these ideas lie speculative and we may uncover many negative impacts of insect farming, however if these actions are looked at in a context-based situation, there may be chance to help discover new and old methods and routines. If anything, we may come together to enjoy a meal together, finding a new recipe which is not always environmentally detrimental, providing a sense of purpose and enjoyment along the way..

# materials map

<iframe
  src="https://embed.kumu.io/fd8beff7b58e855f7d6c3c7e653b2537"
  width="600" height="600" frameborder="0" ></iframe>

# the atlas

  <iframe
    src="https://embed.kumu.io/bb6cd73b84f7e70e4810d5ee065fbab7"
    width="600" height="600" frameborder="0"></iframe>

# final project  

<iframe
  src="https://embed.kumu.io/df2da44cfc064f43f9bb3592f5cb2b3c"
  width="600" height="600" frameborder="0"></iframe>
