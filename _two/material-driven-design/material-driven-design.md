---
title: Material Driven Design
period: 21 January - 22 February
date: 2019-01-21 12:00:00
term: 2
published: true
---

![]({{site.baseurl}}/plastic-wall.jpg)

# the art of craft

If you are ever offered the chance to carve your very own wooden spoon, I would recommend a few things. First of all, patience is a virtue, it sounds cliché however after an hour of faffing with a stick and a blade it may feel like you have been whittling for hours with nothing to show for it but a slightly smoother, well, *stick*.
Secondly, I would recommend a set of plasters, some good plasters as our touchscreen-fashioned hands have now become too smooth for manual labour.

The exercise of carving your own spoon provided the opportunity to illustrate the difference between what you perceive something will look like and what it *actually* looks like. Therapeutic for the mind, sore for the hands, the carving of one small object required an immense amount of human energy to manipulate the material. It is clear to see the almost-meditative state which carving offers, in which the repetitive motion of peeling layer upon layer of wood becomes soothing for the mind. This is perhaps because we live in a world where our attention is fought over, scrabbling to attract our different attention-feedback mechanisms with bright colours, catchy melodies and videos of incredibly pointless things.

The task lay in *really* understanding how to "highlight the quality of the material in the design". How can we rediscover our tangible connection we have to the physical texture of our surrounding environment?

# new material dialogues : yuca

![]({{site.baseurl}}/yuca.jpg)

Behold the mighty yuca, gratefully sourced from the nearby [Leka](https://restauranteleka.com/). The outer two layers however are not so delicious, poisonous to humans in-fact. Despite the fact that wrong preparation of yuca may ruin your day a little, yuca is an incredibly hardy starchy vegetable. Similar to a potato, there are many things that can be done with a yuca, once you have undressed it so to say.

![]({{site.baseurl}}/stages.png)

Yuca thrives in warmer temperatures, poor soil conditions and little water, therefore pushing it's elevated status considering the warmer temperatures the world will receive with climate change.. It mainly originates in South America and Brazil. Yuca may also be seen in it's powdered form, as *tapioca*, or thickening agent due to it's starchiness.

What else is yuca used for aside from cooking? Medicinal uses, animal feed, potential biofuel. It has also been used to produce bioplastics (bags).


![]({{site.baseurl}}/yuca-bits.jpg)


# explorations

*Burning. Grating. Soaking in water. Drying out. Boiling.*

The initial experiments with the material consisted of varying techniques and a little creativity - until a product of some form was formed. After looking at methods of making cornstarch, or tapioca, I found the drying and grinding method the most suitable for the inner membrane of the yuca, with the potential to produce some form of starchy powder which can be utilised.

Further explorations looked at creating some form or matter from the material. The initial explorations offered potential in creating a powdered substance which could act as a binder, or a material itself. Not resigning myself to my powdered substance, I explored a little more in the boiling and baking of the material, finding a lack of aesthetics or quality in my results.

*Returning to the powder.* Looking at creating bioplastics using the starchy substance derived from the inner membrane. There are abundant bioplastics recipes online, in which I selected a few which appeared to provide pleasing results, and began on my mission to make something functional. The difficulties in making the bioplastic lay in the lack of knowledge of how much starch was in the dried powder, and no idea of how to know aside from multiple tests.

![]({{site.baseurl}}/bowl-grind-dry.jpg)

![]({{site.baseurl}}/blended-pulp.jpg)

![]({{site.baseurl}}/tin.jpg)

![]({{site.baseurl}}/form.jpg)

# form

![]({{site.baseurl}}/string.jpg)


# final reflections

When looking at and working with the yuca, the concept of time became a prominent factor in how we understand the components of design. It was surprising how quickly the idea of the material being *waste* diminished, replaced with a nagging questioning of what are the possibilities of this, or these things? In understanding the basis of our world as a set of polymers, a series of *matter*, I developed an appreciation of the simplicity of natural elements, but also of the immense complexity of our man-made polymers.  

Grinding, smashing, melting, burning, shredding and almost-screaming-at the yuca provided the opportunity to reflect on the iterative process we have in design. We, as designers, often visualise and create beautiful things on a screen, modelled to perfection - these ideas translating our world into imagining a stream of seamlessly designed things. It quickly became evident that what we envisage, is not always what will end up with. Through daily experiments with the material, I often resulted with a sloppy liquid which provided no solid, and definitely did not provide anything beautiful.. Again the concept of time and patience reoccured here. The sloppy material providing a gentle reminder that despite our wild imaginations, we require a lot of investment into understanding the fundamentals of the materials we see everyday. Next time I work on a project, I will address and embrace the ugliness and failure, as they offer a history of how we progress and have progressed throughout our existence.

In the two weeks after experimenting (perhaps if I was a scientist, I would have taken less time), I felt that I had developed a relationship with the yuca. It sounds odd, but I became strangely protective over the pieces I had created - they showed a physical emulation of the development of my knowledge - from a blended pulp, a crumbling bowl, mouldy skin to a starchy powder. Once I had developed the correct 'recipe' which could be utilised to make bioplastics, I began to experiment with the material, soon realising that it took around 3-4 days for it to set. Time, reoccured here, clashing with the pressure we had to produce something 3d for the final presentation.

# bioplastics (ish)

First successful bioplastic.

![]({{site.baseurl}}/bioplastic.gif)

# presentation

![]({{site.baseurl}}/close.jpg)


# actual bioplastic

Round 2 at making bioplastics with the perfected recipe, placed into molds to see if suitable. Resulted with a cute small heart, and a cup which I have used as a planter. In hindsight I was using my surrounding equipment to make form, however future use of this bioplastic could be items which need to be waterproof and flexible. For example - bowls, bags or covers.

![]({{site.baseurl}}/heart.jpg)

![]({{site.baseurl}}/dog-plant.jpg)

![]({{site.baseurl}}/plant-wall.jpg)

![]({{site.baseurl}}/two-plants.jpg)

![]({{site.baseurl}}/scrunch-fold.gif)




# recipe

Recipe:
- 1 tbsp starch
- 1 tsp vinegar
- 1 tsp glycerin
- 4 tbsp water.

Heat in a pan on a medium temperature for around 10 minutes until consistency changes to a very sticky glue-like mixture. *Adding more glycerin offers a thicker, flexible bioplastic however takes longer to dry.* Pour into a mold and leave to set for at least 3 days - silicon works well as a mold.

# interesting things


[A Cassava Revolution Could Feed the World’s Hungry](https://www.scientificamerican.com/article/a-cassava-revolution-could-feed-the-worlds-hungry/).

[Cassava as a biofuel.](https://www.scientificamerican.com/article/ethanol-scheme-bids-to-clean-up-cooking-in-mozambique/)

[Cassava crop development in Colombia.](http://www.fao.org/docrep/007/y5271e/y5271e08.htm)

<iframe src="https://e.issuu.com/anonymous-embed.html?u=emilywhyman&d=mdd-presentation" style="border:none;width:100%;height:500px;" allowfullscreen></iframe>
*Presentation.*
