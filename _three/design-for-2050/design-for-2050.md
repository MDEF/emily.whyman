---
title: design for 2050
period: 23 April - 03 May
date: 2019-01-21 12:00:00
term: 3
published: true
---


![]({{site.baseurl}}/letter-2050.svg)

# why are we here?

A guide to positive futures could also be another name for Designing for 2050 - allowing its application to be spread into further futures, or closer presents. In creating the next series of [Black Mirror ](https://www.imdb.com/title/tt2085059/)we were able to consider all possible extents of our projects, from the more sinister to the most positive affect. Despite our episodes not quite at the dramatic effects level (or budget for that matter), the 5 punchy episodes were all impressive in the time constraints and in offering reflection of the world we are actively and passively consuming. The boundaries of design and the boundaries of problems are interrelated, leading back to the question of whether our design is good design, if it is good design, then who is it good for and why?

In bringing this overall view into the perspective of my final project, it is possible to see the positive and negative effects of context-based agriculture in 2050-something or the other. Beginning in Barcelona, I could envisage self-productive warehouses connected with roof gardens; collective recipe sharing websites; shared multipurpose spaces and storage to facilitate testing new ideas and gathering the ideas which were successful; and finally hybrid food labs which also offer spaces for meals. In a way, this knowledge sharing has already begun. The aeroponics system in which I work on with [Jess](https://mdef.gitlab.io/jessica.guy/aeroponics/) is in fact supplied by [Nextfood](https://nextfood.co/community/), an open-source plant powered revolutionary formulated in Copenhagen. The Fab Lab movement has proved making these networks is possible. The idea for the project for the next few years would be to begin hacking the infrastructure of the city - finding spaces which could be planted and harvested, creating environments which are not dominated by the human desires. I see my role here as to push these visions into shape, through creating and providing experiences which allow time for these scenarios to be pondered on, behaviours changed and awareness created. The idea of implementing agriculture within everydayness of the city is not a new idea, or unreachable. It is not the sole answer to food production in a world in which it's inhabitants are predicted to reach [9.8 billion](https://www.un.org/development/desa/en/news/population/world-population-prospects-2017.html) by 2050, but more a methodology to create awareness of where our food comes from. A food-lab network also offers a space for education, a right to knowledge of consumption. This is broadly speculative, but it may also reduce cases of depression, diabetes and other health-related issues - from the increased nutrients in locally produced food, to reduction in packaging waste and also providing new livelihoods to future farmers.

I would like to add a special note as thank you to Andrés, whether you read this or not. It is not often that the teacher is also the student in the classroom, and the students are also teachers. It's also not often that presentations are given in GIF formats, or narratives formulated from GIF's alone. The course content, structure and presentation itself offered a different format to standard educational schooling, however worked incredibly well at engaging our attention and making us think. Perhaps we're all just a bit strange and quirky educational tools suit us well, but I would recommend trying GIF presentations, GIF videos, or even creating a new series of Black Mirror out at least once.

# looking for silence

<iframe src="https://player.vimeo.com/video/337008751" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/337008751">'Looking for Silence' - our Black Mirror take</a> from <a href="https://vimeo.com/user98671362">Emily Whyman</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
