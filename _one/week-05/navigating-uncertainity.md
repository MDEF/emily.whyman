---
title: Navigating the Uncertainty
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

{% figure caption: "Andrés Jaque - *Ecologizar no solamente es verdear* (2008). " %}
![]({{site.baseurl}}/andres jaque.jpg)
{% endfigure %}

The past 5 weeks of MDEF have offered a variety of complex topics which have questioned the path that I *thought* I would follow, throwing the odd sobering hurdle in the way, but it has also emboldened a drive to be a changemaker and flourish in our own individuality. *Navigating the Uncertanity* questioned the agency of things in the domesticated world of the anthropocene.

How and **why** have we reached the society in which we live in today? Who has dictated our primacy of the human race, immersion and addiction to immediate consumerism? What are their **consequences?**

Leaving the first talk a little pessimistic, in a slight crisis of confidence in what I believe in, I questioned whether I should discard my values and run to the nearest McDonald's to request a happy meal. Throughout the majority of my adult life I would consider myself as ecologically minded, a blessing handed down from my parents from a young age. It seemed that our attempts to *save* the planet through cutting out meat, recycling and growing our own vegetables are a vein endeavours to resurrect the mistakes we have already made. Discussing these issues, we questioned **what can we now do, as designers?** Per Espen Stoknes [TED Talk](https://www.ted.com/talks/per_espen_stoknes_how_to_transform_apocalypse_fatigue_into_action_on_global_warming?language=en) offers a more positive outlook on climate change and how we, as designers, can do what we do best - navigate through a narrative and embrace chaos..

*Some positive ideas..*

[Coastal vegetation restoration](https://www.wired.com/story/the-sea-could-save-us-from-ourselves/) may not sound sexy, but it sure is for the environment.

[Cultures of Resilience](http://culturesofresilience.org/wp-content/uploads/resources/CoR-Booklet-forHomePrinting.pdf) *"risk taking and chaos embracing"* and *"managing chaos not cleaning up mess"*..

 ["Design for Dissassembly"](https://www.bbc.com/news/business-45969676).

## actor-network theory


![]({{site.baseurl}}/social_life.jpg)


William H. Whyte's 'Social Life of Small Urban Spaces' is a good way to start investigating the agency of things - how does the design of objects influence our behaviours? The video and book explores the success and failure of New York's Seagram Plaza, suggesting reasons as to why they encourage mindless dawdling or not. Factors include positioning of 'perching' spots in relation to sun exposure, nearby facilities, and best of all - ability to people-watch.

![]({{site.baseurl}}/people_watching.png)

It was a relief to hear the 'term actor-network theory' (ANT) - a concept which I have studied in-depth previously. ANT provides a methodology to view and understand aspects, places and symbols in society in which we can use as a foundation to begin subtly designing in order to change the way we act. *'Nudging'* is a form of altering habit change - and has been successful in behavioural changes towards the environment. For example, the increased availability of vegetarian and vegan products has led to an huge increase in numbers of vegetarians over the past few decades. Whether it be for health, monetary, or environmental reasons, the meatless option is empowering to the individual. I am beginning to question how I can offer 'tools of empowerment' through creating an open-source design process that is comprehensible at various scales. ['Aquapioneers'](http://aquapioneers.io/) is a great example of this, a website which has made available an downloadable design to start your own future fishy utopias.

{% figure caption:"[Making things Visible](https://issuu.com/emilywhyman/docs/all_combined_small)" caption%}.
![]({{site.baseurl}}/mtv.gif)
{% endfigure %}


*"A clear definition is needed of the components of ANT to understand transformation of space. An actor, is better termed an actant due to the objective nature that actor implies. An actant may be human or non-human and is something that is made to act. “A network is a concept describing the traces left behind by moving actors” (Fallan, 2011, p82). A network is a product of actors which agree to sustain it to meet a need – the process of translation."*



## scales, cycles, interfaces + symbiotics



*"How much capacity do we have to push ourselves to test the limits of our own abilities?"*
[Anab Jain](http://superflux.in/index.php/work/how-will-we-live/#)

Anab Jain reflects an optimistic, yet realistic, inspiring designer. Anab questions the power we have to influence our future, through specific technologies but also through semantics. Our 'selective exposure' to the problems climate change poses questions regarding how can we create products and platforms which are considerate towards the future, but also do not require noticeable personal sacrifice? For example, city centre living is often considered hectic, loud and lacking green space - how can we break our habitual feedback loops, designing into *what already exists* to encourage biodiversity, and for the city to not feel so tiring and exploitable? How can we extend our interfaces to encourage healthy bacterial growth?

*"We are born with an inherited aesthetic tendency to appreciate an intimate connection with the world.. 'We yearn for connection with one another, and with the soul, but we forget that, like the earthworm, we too are an organism of the soil. We too need grounding.'"*

(Thackara, 'How to Thrive in the Next Economy', p33.)

We are now designing for new parameters - Thackra illustrates how developed countries have in-fact glossed and embellished products and 'green' systems of 'resilience' whereas the global South, where countries are already being hit by impacts of climate, are producing sensitive intellectual ideas in response. I'm interested in **symbiotic** **exchange**. [Biotope City](http://www.biotope-city.net/) is a positive project which looks at the city as a "specific form of nature". I'm interested in investigating the opportunities from domestication of the planet, the waste we produce, and *embracing chaos* in order to evolve new ideas.

## waste

![]({{site.baseurl}}/waste_collage.jpg)

*"Our wasteful patterns of consumption would soon change if we saw, heard, smelled, tasted and felt all this litter, rubbish and trash as 'lively' - not just inert stuff."*

(Thackara, p162)

We choose what we perceive as waste. The past 5 weeks have also altered my view of waste - i'm interested in developing the notion of *what* is waste for one organism may in fact feed another. After all, we all thrive on the idea of excitement in the unexpected places. So what if an underused car park could offer new form of biodiversity, or through trash-hunting, we can create a DIY hydroponics system? I plan on blurring the lines between what we perceive as natural and unnatural, changing the agency and purpose of objects to provide new biodiverse life. This relates back to my statements in the first week - the necessity to create dense, liveable city spaces for not only humans. By focussing on waste, it is possible to ask questions about our behaviours, and ultimately, how we consciously design.

![]({{site.baseurl}}/3 circles.jpg)

## books, quotes, videos and other nice things

### "creative anxiety"


'Creative Anxiety' - inspiration if you feel a little lost in the design - world.
Even has a nice online [PDF.](https://thecreativeindependent.com/library/on-dealing-with-creative-anxiety/)

*"the zone of proximal development" - "In children,
the zone of proximal development is the
area in your own cognition where you’re just
capable enough, but not perfectly capable,
so that there’s a little bit of a stretching,
where you have to work a little bit to get
something, but then the reward is really big."*

(P13)

[The Lonely City](https://www.goodreads.com/book/show/25667449-the-lonely-city?from_search=true): Adventures in the Art of Being Alone.

### political design


[Nudge: Improving Decisions about Health, Wealth and Happiness](https://www.goodreads.com/book/show/3450744-nudge?from_search=true).

*"The environment as an outcome of a global choice architecture system in which decisions are made by all kinds of actors, from consumers to large companies to governments. Markets are a big part of this system, and for all their virtues, they face two problems that contribute to environmental problems."*

(p185)

The costs that we impose we do not physically see. Require better incentives: pollution tax, cap-and-trade system. Improve process of feedback. Large-scale companies avoiding environmental blacklisting due to bad publicity. Greenhouse Gas Inventory (GGI), positive environmental labelling (relates to social constructivity / feedback).

Ambitious environmental nudges : The'Ambient Orb' which glows red when using a lot of energy.

['The Power of Behavioural Design: Looking Beyond Nudging.'](https://www.theguardian.com/sustainable-business/power-behavioural-design-beyond-nudging).

### optimism


[How to Thrive in the Next Economy](https://www.goodreads.com/book/show/25690403-how-to-thrive-in-the-next-economy).

[The Great Recovery](http://www.greatrecovery.org.uk/).

[Human Flow](https://www.humanflow.com/).

### waste

[Lessons for the 21st Century](https://www.ynharari.com/book/21-lessons/) Harari.

["The century of the self"](https://www.imdb.com/title/tt0432232/) - Documentary.

*"It's possible to develop products that make people perform irrational when a product meets their emotional desires" "irrelevant objects became powerful emotional symbols of how you wanted to be seen by others."*

[A Sideways Look at Time](https://www.goodreads.com/book/show/15787.A_Sideways_Look_at_Time), Jay Griffiths.

[The Nature of Cities](https://www.thenatureofcities.com/)

<iframe frameborder="0" scrolling="no" height="130" width="100%" src="https://www.wnyc.org/widgets/ondemand_player/wnycstudios/#file=/audio/json/648425/&share=1"></iframe>
