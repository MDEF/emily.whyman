---
title: Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/phoebe_deer.jpg)

# a narrative about narratives

How do you tell a story about a story? Start with a week of *Engaging Narratives* - and punctuate with a few other interesting snippets of things. This narrative relates to the wider vision of the future for the masters and further, a vision for the big-wide-world after IAAC. This is a story about pledging for *designs to come* - which you will not even realise you desired or needed, as theoretically, they do not *exist* yet. Call it an exhibition of the future..

*The photo from above is a film photo from a fancy dress party last year. It might seem a little strange however it's my friend, Phoebe, dressed up as Bambi. It represents a narrative in two ways. First of all, the film is a narrative in itself - the photo captured lies mysterious, unrevealed and unknown until developed, the excitement and memories re-evoked in its exhibition. The second is the outfit, representing the fictional character - Bambi - a rather harrowing tale from the 1940's of a young deers plight to grow up in the forest. I remember repeatedly watching Bambi on VHS when I was younger, a memory which stands as a reminder of how much technology has progressed in mere decades..*

# from smoothies to ceramics
*- A few examples of engaging narratives.*

[Innocent Smoothies](https://www.innocentdrinks.co.uk/us/sustainability) stand out as an incredibly effective methodology for selling a product, which is essentially, fruit which you can blend at home to make pretty much the same - well, a smoothie. They are transparent in selling the simplicity of their smoothies, even listing the amount of each ingredients which go into their rather delicious fusions. It's almost *encouraging* you to try and make the same smoothie, see if it tastes as good. The Innocent website is as honest as it sounds: offering a history of the product; articulating their environmental awareness; a blog and even a section for those who are bored (I would recommend the 'pencil drawing' section).

![]({{site.baseurl}}/banana-phone.png)


Smoothies aside, we move on to the heroic tale of the saviour of Granby Street, Liverpool. Originally a disregarded and forgotten area, Granby Street is an illustration of bottom-up successes which a large social profile and following has aided greatly. Residents became fatigued with a lack of council response and odious sights of rubbish, taking a mission upon their hands to reinvent the street with a trowel, some plants, and cheap cups of tea in fashionably British spirit. Granby Street has its own experimental [ceramics](https://granbyworkshop.co.uk/) workshop, creating individual, handcrafted pieces which each display their own marks of their creators.

Both Granby Street and Innocent Smoothies have an honest, empathetic narrative. They advocate their equal status - we are all weary consumers looking for some authenticity in purchasing power. They offer a humanistic, playful element in their design. And it works.

# a narrative for the future

Ironically, this is going to start with a recount of the past. But it leads to the belief which certain ideas should exist in the world, as they foreground the designs for the future, or present. This has all been discovered during the iterative weeks of MDEF. Cultivating bacteria questioned self-replicating processes in the city, otherwise known as *fractals*. Scrambling around bins led to investigating the informal economies within Barcelona and potential reuse of materials which are considered 'waste'. Dissecting printers led to a deeper understanding of the complexity of materials we use and *where* they actually come from, and moreover, the monumental distances they have travelled. Debating Plato and Aristotle's theories around *what* constitutes intelligence and consciousness has questioned how our habits resonate with society today and the rather probing question of *what does it actually mean to be human?* This multitude of different components is an attempt at *sense-making* of the current normal in society and what can be forecast as the *new normal.*  So what should design do?

Design should probe people.

It should poke questions in what you believe in, whether consciously or subconsciously, it should offer a metaphorical nudge which may guide you in your daily scenarios. Design should be optimistic, but also opportunistic, offering enlightenment when there may be moments of despair and disrepair in the world.

![]({{site.baseurl}}/poke.svg)


# roots

Although hypothetical, 'Roots' is a food collective which uses food as a common language - a language which we all speak and need. Returning to my aim to become political in design, alter our habits through behavioural nudges and create awareness of the potential environment, Roots intends on creating food collectives within the city and an awareness around what we put on our plates.

Roots may be a fictional kickstarter-style narrative, however it outlines the path I wish to follow, regarding consumption habits, behaviour, knowledge of what we buy, where it comes from, and further, the potential possibilities for future urban densification.

![]({{site.baseurl}}/total.gif)

# food for thought

[How one community beat the system, and rebuilt their battered streets.](https://www.theguardian.com/commentisfree/2018/feb/14/community-liverpool-residents-granby)

[Engaging refugee narratives.](http://www.refugeenarratives.com/about-june-2017)
