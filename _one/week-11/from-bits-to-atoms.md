---
title: From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-16 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/google.png)



# sassiest ever

Introducing another adaptation to plant life: [Sassy Plant](https://mdef.gitlab.io/sassy-plants/), aka plant-bot will follow your movements. It was also intended to have a mechanical spinning blade which would ensure you never forgot to water it, or go too near its delicate leaves. Due to a shortage of time and a lack of a blade we were unfortunately unable to implement our grand schemes for our sassy plant.

Next time.

![]({{site.baseurl}}/sassy-plant.jpg)



# from root to leaf

How did we get to the ridiculous idea of the sassy plant? Inspired by robot wars, we proceeded to place friendlier face on killer robots. Initial discussions on how we could make our plant **do something else** ranged from a self-chopping herb slicer, to a talking plant, using object recognition via a Connect, until we opted for the more modest idea of making the plant move, directed via a separate controller (laptop or phone).

{% figure caption: "the tv classic - **robot wars.**" caption%}
<iframe width="640" height="360" src="https://www.youtube.com/embed/ASKJsHa5n2U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
{% endfigure %}


# robotic reflections

The week of Bits to Atoms offered the opportunity to apply our electronic education to a physical thing, albeit be a rather amusing and not-so-functional plant. My specific help lay in the realm of Autocad design of the plant-bot's exterior casing, construction, helping with the wiring and photography. In the future I would ideally like to have more experience in the coding realm, for any further sassy-plant antics. Acting as an introduction to the future fab academy course, the week has provided infinite fun to play and gather ideas for our future projects: adding more depth and meaning to what we plan on accomplishing. It would be fabulous to create Sassy Plant No.2, perhaps with the spinning blades this time..

![]({{site.baseurl}}/circuits.JPG)
![]({{site.baseurl}}/car.JPG)
![]({{site.baseurl}}/kat.jpg)
![]({{site.baseurl}}/wheel.jpg)
![]({{site.baseurl}}/sticker.jpg)




## to make your own sassy plant visit the repository on gitlab

[Sassy Plant](https://mdef.gitlab.io/sassy-plants/).
